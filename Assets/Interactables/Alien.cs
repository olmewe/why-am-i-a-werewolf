﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien : DialogueInteractable
{
    protected override IEnumerator StartDialogue()
    {
        SetSprite(0);
        yield return NPCText("human, or whatever you are, take me to your leader");
        yield return WolfText("i'm the leader", "no");
        if (dialogue.selectedIndex == 0)
        {
            yield return NPCText("wow... i never thought i'd get this far");
        }
        else
        {
            yield return NPCText("wow... that's new");
        }
    }
}
