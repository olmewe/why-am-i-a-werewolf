﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antinoise : DialogueInteractable
{
    bool unlocked = false;

    protected override IEnumerator StartDialogue()
    {
        if (unlocked) yield break;

        yield return GetItem("ear muffs", true);
        unlocked = true;
        SetSprite(0);
    }
}
