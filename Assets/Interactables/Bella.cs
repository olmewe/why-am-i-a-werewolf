﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bella : DialogueInteractable
{
    protected override IEnumerator StartDialogue()
    {
        SetSprite(0);
        yield return NPCText("(shock) wolf!!!");
        yield return WolfText("human!", "...");
        if (dialogue.selectedIndex == 0)
        {
            yield return NPCText("...am i really? you make the line between what makes one human and wolf very thin...");
        }
        yield return NPCText("...you’re not so scary...");
        yield return WolfText("i’m a good boy", "(does a scary face)");
        if (dialogue.selectedIndex == 0)
        {
            yield return NPCText("i know i can trust you!");
            yield return WolfText("thanks");
            yield return WolfText("i trust you too");
        }
        else
        {
            yield return NPCText("aaaa!");
        }
    }
}
