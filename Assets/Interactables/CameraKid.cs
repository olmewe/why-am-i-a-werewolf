﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraKid : DialogueInteractable
{
    protected override IEnumerator StartDialogue()
    {
        SetSprite(0);
        yield return NPCText("hi! can i pretty please take your picture?");
        yield return WolfText("yes", "no");
        if (dialogue.selectedIndex != 0) yield break;
        yield return NPCText("*flash*");
        yield return GetItem("picture", true);
        canInteract = false;
    }
}
