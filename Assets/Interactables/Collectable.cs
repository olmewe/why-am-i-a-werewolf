﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : DialogueInteractable
{
    [Header("collectable")]
    public string itemName;
    public Collider2D hitbox;

    protected override IEnumerator StartDialogue()
    {
        yield return GetItem(itemName, true);
        canInteract = false;
        sprite.enabled = false;
        hitbox.enabled = false;
    }
}