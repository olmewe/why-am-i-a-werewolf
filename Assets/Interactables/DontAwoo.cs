﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontAwoo : DialogueInteractable
{
    protected override IEnumerator StartDialogue()
    {
        yield return NPCText("DON'T AWOO\n$500 PENALTY");
        yield return WolfText("awoo", "don't awoo");
        if (dialogue.selectedIndex == 0) Fee.instance?.AddFee(500);
    }
}
