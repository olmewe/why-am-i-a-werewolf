﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frango : DialogueInteractable
{
    bool unlocked = false;

    protected override IEnumerator StartDialogue()
    {
        if (unlocked)
        {
            yield return NPCText("♫");
            yield break;
        }

        yield return NPCText("♫ saudade... ♫");
        yield return NPCText("♫ saudade... ♫");
        yield return NPCText("♫ saudade... ♫");

        yield return WolfText("continue listening", "leave");
        if (dialogue.selectedIndex != 0) yield break;

        yield return NPCText("♫ talvez não tenha tanto medo assim... ♫");
        yield return NPCText("♫ de trovão... ♫");
        yield return NPCText("♫ talvez não tenha tanto medo assim... ♫");
        yield return NPCText("♫ de trovão... ♫");
        
        yield return WolfText("continue listening", "leave");
        if (dialogue.selectedIndex != 0) yield break;

        yield return NPCText("♫ saudade!... ♫");
        yield return NPCText("♫ saudade... ♫");
        yield return NPCText("♫ saudade. ♫");
        
        yield return GetItem("chicken leg", true);
        unlocked = true;
    }
}
