﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fursuiter : DialogueInteractable
{
    protected override IEnumerator StartDialogue()
    {
        yield return WolfText("(gasp) !!!");
        yield return NPCText("(stares with static eyes)");
        yield return WolfText("(stares back)");
        yield return NPCText("nice suit you got there!");
        yield return WolfText("thanks!", "but i’m naked");
        yield return NPCText("you’re really in character haha");
        yield return WolfText("what?");
        yield return NPCText("yes! i mean you look so realistic! i can’t even believe there’s a person inside all that dirt-looking fur!");
        if (Wolf.cameToTerms)
        {
            yield return WolfText("oh, there isn't!");
            SetSprite(0);
            yield return NPCText("i wish i had your attitude!");
        }
        else
        {
            yield return WolfText("i can’t wait to be a person again");
            SetSprite(0);
            yield return NPCText("same! i mean it’s too hot isn’t it?");
        }
    }
}
