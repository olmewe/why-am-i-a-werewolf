﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Janitor : DialogueInteractable
{
    protected override IEnumerator StartDialogue()
    {
        SetSprite(0);
        yield return NPCText("that’s a damn fine looking mop.");
        yield return WolfText("excuse me?", "(knock over garbage can)");
        yield return NPCText("ah, sorry. didn’t mean harm. i’m just tired of trying to clean with this naked stick.");
        yield return NPCText("if i could i’d use another, but the boss don’t care. how ‘bout lending me some hair? haha,");
        yield return WolfText("good one.", "(knock over garbage can)");
        yield return NPCText("i know. don’t flatter me. heh.");
        yield return NPCText("y’know, kid. some things never change. i’ll always have this mop. i can’t do nothing ‘bout that.");
        yield return WolfText("can’t you really?", "that sucks. i get you (lends some loose fur)");
        if (dialogue.selectedIndex == 0)
        {
            yield return NPCText("it’s a metaphor ok?");
        }
        yield return NPCText("...\nhere, take this");
        
        yield return GetItem("dog plushie", true);

        yield return NPCText("they never came back to rescue it. give it a happy life, will you?");
        yield return WolfText("will do.");

        canInteract = false; 
    }
}
