﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : DialogueInteractable
{
    protected override IEnumerator StartDialogue()
    {
        yield return NPCText("@bellaqueberra @kirinsprii @Sugar_Lotte @olmewe @willyofruit were here");
    }
}
