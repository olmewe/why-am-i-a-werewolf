﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reading : DialogueInteractable
{
    [Header("custom")]
    public Sprite[] altIdle;
    
    int currentFact = -1;

    protected override IEnumerator StartDialogue()
    {
        SetSprite(0);

        if (currentFact < 0)
        {
            yield return NPCText("you seem lost, lucky for you, I have knowledge");
            currentFact++;
        }

        if (currentFact < 1)
        {
            yield return NPCText("would you like to know a wolf fact?");
            yield return WolfText("yes", "no");
            if (dialogue.selectedIndex != 0) yield break;
            yield return NPCText("did you know that wolves have their claws showing at all times? they’re not retractable like cats!");
            yield return WolfText("ah....ok");
            yield return NPCText("you could hurt someone with those y’know, good thing there’s pliers for animal claws, do you have any?");
            yield return WolfText("...");
            currentFact++;
        }

        if (currentFact < 2)
        {
            yield return NPCText("wanna hear another one?");
            yield return WolfText("yes", "no");
            if (dialogue.selectedIndex != 0) yield break;
            yield return NPCText("did you know that wolves have the best canine sight at night but are myope?");
            yield return WolfText("...huh");
            yield return NPCText("i guess you can’t just go buy glasses right? haha");
            yield return WolfText("...");
            currentFact++;
        }

        if (currentFact < 3)
        {
            yield return NPCText("wanna hear another one?");
            yield return WolfText("yes", "no");
            if (dialogue.selectedIndex != 0) yield break;
            yield return NPCText("did you know that wolves don’t have reflections? they just don’t!");
            yield return WolfText("...excuse me?");
            yield return NPCText("sorry that was weird, do you have a mirror on you though?");
            yield return WolfText("no...");
            yield return NPCText("you can keep mine!");
            yield return GetItem("mirror", false);
            currentFact++;
        }

        if (currentFact < 4)
        {
            yield return NPCText("wanna hear another one?");
            yield return WolfText("yes", "no");
            if (dialogue.selectedIndex != 0) yield break;
            yield return NPCText("did you know that wolves have a highly organised social structure enabling them to enjoy maximum cooperation when hunting, communicating and defending territory?");
            yield return WolfText("uhh... cool");
            yield return NPCText("Where’s your pack?");
            yield return WolfText("...");
            currentFact++;
        }

        if (currentFact < 5)
        {
            yield return NPCText("wanna hear another one?");
            yield return WolfText("yes", "no");
            if (dialogue.selectedIndex != 0) yield break;
            yield return NPCText("did you know wolves have huge feet? they're just so huge, it's kinda scary");
            yield return WolfText("......sure");
            yield return NPCText("I can't even imagine what it must be like, I love wearing cool socks I don't think I could handle it");
            yield return WolfText("...");
            currentFact++;
        }

        if (currentFact < 6)
        {
            yield return NPCText("wanna hear another one?");
            yield return WolfText("yes", "no");
            if (dialogue.selectedIndex != 0) yield break;
            yield return NPCText("did you know that wolves have great hearing?");
            yield return WolfText("hm");
            yield return NPCText("I bet this quiet park is very loud for a wolf, it's a pity, such a nice place to relax in");
            yield return WolfText("...");
            currentFact++;
        }

        if (currentFact < 7)
        {
            yield return NPCText("wanna hear another one?");
            yield return WolfText("yes", "no");
            if (dialogue.selectedIndex != 0) yield break;
            yield return NPCText("did you know that wolves are very smart?? their brains are pretty big for a canine and they are known for being strategic hunters!");
            yield return WolfText("alright");
            yield return NPCText("I bet wolves have good memories too, weirdly enough, I have the feeling you don’t even know who you are, am I right?");
            yield return WolfText("...");
            currentFact++;
        }

        yield return NPCText("wanna hear another one?");
        yield return WolfText("yes", "no");
        if (dialogue.selectedIndex != 0) yield break;
        yield return NPCText("did you know the cranium of the wolf has teeth perfect for bone crushing?");
        yield return WolfText("(i’m thinking about crushing your bones)");
        yield return NPCText("...");
        yield return WolfText("....");
        yield return NPCText("...");
        yield return WolfText("...");
        yield return NPCText("i guess you are tired of wolf facts....");
        yield return WolfText("i am");
        yield return NPCText("actually i’m not even reading that book anymore... i even made up some of the facts");
        yield return WolfText("...oh");
        yield return NPCText("here... you can have it");
        
        SetSprite(1);
        idle = altIdle;

        yield return GetItem("book about wolves", true);

        canInteract = false;
    }
}
