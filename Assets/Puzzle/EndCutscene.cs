﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndCutscene : DialogueInteractable
{
    protected override IEnumerator StartDialogue()
    {
        yield return WolfText("that's... me?");
        yield return WolfText("that's me... as a baby...");
        yield return WolfText(".....");
        yield return WolfText("it's difficult to accept that i was never even human after all.");
        yield return WolfText("especially after trying so hard to become one.");
        yield return WolfText("after spending my entire life doing this.");
        yield return WolfText("...");
        yield return WolfText("it'll take some time for me to get used to this.");
        yield return WolfText("for now i can at least enjoy the scenery.");
        Wolf.cameToTerms = true;
        Puzzle.instance?.StopEndCutscene();
    }
}
