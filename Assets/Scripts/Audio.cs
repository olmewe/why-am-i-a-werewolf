﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public static Audio instance { get; private set; }

    [Header("sources")]
    public AudioSource bgm;
    public AudioSource speech;
    public AudioSource sfx;

    [Header("clips")]
    public AudioClip select;
    public AudioClip jingle;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        bgm.Play();
    }

    public void StartSpeech()
    {
        speech.loop = true;
        if (!speech.isPlaying) speech.Play();
    }

    public void StopSpeech()
    {
        speech.loop = false;
    }
    
    public void PlaySelect() => sfx.PlayOneShot(select);
    public void PlayJingle() => sfx.PlayOneShot(jingle);
}
