﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public enum Mode
    {
        Keyboard,
        Mouse,
        Touch,
    }
    public static Mode mode { get; private set; }

    readonly public static KeyCode[] up = { KeyCode.W, KeyCode.UpArrow };
    readonly public static KeyCode[] down = { KeyCode.S, KeyCode.DownArrow };
    readonly public static KeyCode[] left = { KeyCode.A, KeyCode.LeftArrow };
    readonly public static KeyCode[] right = { KeyCode.D, KeyCode.RightArrow };
    readonly public static KeyCode[] action = { KeyCode.Return, KeyCode.KeypadEnter };
    readonly public static KeyCode[] menu = { KeyCode.Tab };

    public static KeyCode[] any {
        get {
            if (m_any == null)
            {
                var list = new List<KeyCode>();
                list.AddRange(up);
                list.AddRange(down);
                list.AddRange(left);
                list.AddRange(right);
                list.AddRange(action);
                list.AddRange(menu);
                m_any = list.ToArray();
            }
            return m_any;
        }
    }
    static KeyCode[] m_any = null;

    public static bool Press(KeyCode[] keys)
    {
        for (int a = 0; a < keys.Length; a++)
        {
            if (Input.GetKeyDown(keys[a])) return true;
        }
        return false;
    }

    public static bool Hold(KeyCode[] keys)
    {
        for (int a = 0; a < keys.Length; a++)
        {
            if (Input.GetKey(keys[a])) return true;
        }
        return false;
    }

    public static bool Release(KeyCode[] keys)
    {
        for (int a = 0; a < keys.Length; a++)
        {
            if (Input.GetKeyUp(keys[a])) return true;
        }
        return false;
    }

    public static Vector2 axis {
        get {
            var axis = new Vector2
            {
                x = (Hold(right) ? 1 : 0) - (Hold(left) ? 1 : 0),
                y = (Hold(up) ? 1 : 0) - (Hold(down) ? 1 : 0),
            };
            var sqrMagnitude = axis.sqrMagnitude;
            if (sqrMagnitude > 1)
            {
                axis /= Mathf.Sqrt(sqrMagnitude);
            }
            return axis;
        }
    }

    public static bool pointerAvailable { get; private set; }
    public static bool pointerNew { get; private set; }
    public static Vector2 pointerPosition { get; private set; }
    public static bool pointerPositionUpdated { get; private set; }

    static int fingerId;

    void Start()
    {
        Input.simulateMouseWithTouches =
#if UNITY_EDITOR
            true;
#else
            false;
#endif

        pointerAvailable = false;
        pointerNew = false;
        pointerPosition = Vector2.zero;
        pointerPositionUpdated = false;
        
        mode = Input.touchSupported ? Mode.Touch : Mode.Keyboard;
    }

    void Update()
    {
        if (Press(any)) mode = Mode.Keyboard;
        
        var pointerPositionPrev = pointerPosition;
        if (Input.touchSupported)
        {
            pointerNew = false;
            for (int a = 0; a < Input.touchCount; a++)
            {
                var touch = Input.GetTouch(a);
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        pointerNew = true;
                        if (!pointerAvailable)
                        {
                            pointerAvailable = true;
                            pointerNew = true;
                            mode = Mode.Touch;
                            pointerPosition = touch.position;
                            fingerId = touch.fingerId;
                        }
                        break;

                    case TouchPhase.Moved:
                    case TouchPhase.Stationary:
                        if (pointerAvailable && fingerId == touch.fingerId) pointerPosition = touch.position;
                        break;
                        
                    case TouchPhase.Canceled:
                    case TouchPhase.Ended:
                        if (pointerAvailable && fingerId == touch.fingerId) pointerAvailable = false;
                        break;
                }
            }
        }
        else
        {
            pointerAvailable = Input.GetMouseButton(0) || Input.GetMouseButtonUp(0);
            pointerNew = Input.GetMouseButtonDown(0);
            pointerPosition = Input.mousePosition;
        }

        ClampPointerPosition();
        pointerPositionUpdated = pointerNew || pointerPosition != pointerPositionPrev;
        if (pointerPositionUpdated && !Input.touchSupported) mode = Mode.Mouse;
    }

    void ClampPointerPosition()
    {
        var pos = pointerPosition;
        pos.x = Mathf.Clamp(pos.x, 0, Screen.width);
        pos.y = Mathf.Clamp(pos.y, 0, Screen.height);
        pointerPosition = pos;
    }
}
