﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float acc = 4;

    void Start()
    {
        Snap();
    }

    void LateUpdate()
    {
        var pos = transform.position;
        pos.x = Mathf.Lerp(pos.x, target.position.x, Time.deltaTime * acc);
        pos.y = Mathf.Lerp(pos.y, target.position.y, Time.deltaTime * acc);
        transform.position = pos;
    }

    public void Snap()
    {
        var pos = transform.position;
        pos.x = target.position.x;
        pos.y = target.position.y;
        transform.position = pos;
    }
}
