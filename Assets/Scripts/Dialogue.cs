﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    public static Dialogue instance { get; private set; }

    [Header("properties")]
    public float characterVelocity;
    public float unselectedTextAlpha;
    public Color backgroundDefault;
    public Color backgroundWolf;

    [Header("internal references")]
    public GameObject content;
    public Image background;
    public GameObject overlay;
    public Image overlayImage;
    public List<Text> text;

    int contentCount;
    float characterTempo;
    string contentScroll;
    string inputText;

    Sprite[] overlaySprites;
    const float overlayFrameDuration = .5f;
    float overlayTempo;
    int overlayIndex;

    public bool showingDialogue => content.activeSelf;

    public int selectedIndex { get; private set; }
    public string selectedText { get; private set; }

    void Awake()
    {
        instance = this;

        HideText();
    }

    void Update()
    {
        bool playSfx = false;
        if (showingDialogue)
        {
            if (contentCount <= 0)
            {
                var chars = Input.inputString;
                for (int a = 0; a < chars.Length; a++)
                {
                    var c = chars[a];
                    if (c == '\n' || c == '\r') continue;
                    if (c == '\b')
                    {
                        if (inputText.Length > 0)
                        {
                            inputText = inputText.Substring(0, inputText.Length - 1);
                        }
                        continue;
                    }
                    if (char.IsWhiteSpace(c))
                    {
                        if (inputText.Length > 0 && !char.IsWhiteSpace(inputText[inputText.Length - 1]))
                        {
                            inputText += ' ';
                        }
                        continue;
                    }
                    inputText += c;
                }
                text[0].text = inputText;
                if ((Time.time % 1) < .5f) text[0].text += '|';
                selectedText = inputText.TrimEnd();
            }
            else if (contentCount == 1)
            {
                if (characterTempo < contentScroll.Length)
                {
                    characterTempo += Time.deltaTime * characterVelocity;
                    if (characterTempo >= contentScroll.Length)
                    {
                        text[0].text = contentScroll;
                    }
                    else
                    {
                        int length = Mathf.CeilToInt(characterTempo);
                        text[0].text = contentScroll.Substring(0, length);
                    }
                    playSfx = true;
                }
            }
            else
            {
                int newIndex = selectedIndex;
                int motion = (Button.Press(Button.right) ? 1 : 0) - (Button.Press(Button.left) ? 1 : 0);
                if (motion != 0)
                {
                    int oldIndex = Mathf.Clamp(selectedIndex, 0, contentCount - 1);
                    newIndex = Mathf.Clamp(oldIndex + motion, 0, contentCount - 1);
                }
                if (Button.mode == Button.Mode.Mouse && Button.pointerPositionUpdated)
                {
                    int i = FindTextIndexThroughPointer();
                    if (i >= 0) newIndex = i;
                }
                if (selectedIndex != newIndex)
                {
                    selectedIndex = newIndex;
                    selectedText = text[selectedIndex].text;
                    UpdateSelectedText();
                    Audio.instance?.PlaySelect();
                }
            }
            if (overlaySprites != null && overlaySprites.Length > 1)
            {
                overlayTempo += Time.deltaTime;
                if (overlayTempo >= overlayFrameDuration)
                {
                    overlayTempo = 0;
                    overlayIndex = (overlayIndex + 1) % overlaySprites.Length;
                    overlayImage.sprite = overlaySprites[overlayIndex];
                }
            }
        }
        if (Audio.instance)
        {
            if (playSfx)
            {
                Audio.instance.StartSpeech();
            }
            else
            {
                Audio.instance.StopSpeech();
            }
        }
    }

    void EnsureTextCount(int count)
    {
        while (text.Count < count)
        {
            var obj = Instantiate(text[0].gameObject).transform;
            obj.SetParent(text[0].transform.parent);
            obj.localRotation = Quaternion.identity;
            obj.localScale = Vector3.one;
            text.Add(obj.GetComponent<Text>());
        }
    }

    public bool EnsureCanAdvanceWithPointer()
    {
        if (contentCount <= 1) return true;
        int i = FindTextIndexThroughPointer();
        if (i < 0) return false;
        selectedIndex = i;
        selectedText = text[selectedIndex].text;
        UpdateSelectedText();
        return true;
    }

    int FindTextIndexThroughPointer()
    {
        for (int a = 0; a < text.Count; a++)
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(text[a].rectTransform, Button.pointerPosition, Camera.main))
            {
                return a;
            }
        }
        return -1;
    }

    public void ShowNPCText(string content, bool italic = false)
    {
        this.content.SetActive(true);
        background.color = backgroundDefault;
        contentCount = 1;
        EnsureTextCount(1);
        for (int a = 0; a < text.Count; a++)
        {
            text[a].gameObject.SetActive(a == 0);
        }
        text[0].text = "";
        text[0].fontStyle = italic ? FontStyle.Italic : FontStyle.Normal;
        contentScroll = content;
        characterTempo = 0;
        selectedIndex = 0;
        selectedText = content;
        UpdateSelectedText();
    }

    public void ShowWolfText(params string[] content)
    {
        if (content.Length < 1) return;
        this.content.SetActive(true);
        background.color = backgroundWolf;
        contentCount = content.Length;
        EnsureTextCount(content.Length);
        for (int a = 0; a < contentCount; a++)
        {
            text[a].gameObject.SetActive(true);
            text[a].text = content[a];
            text[a].fontStyle = FontStyle.Normal;
        }
        for (int a = contentCount; a < text.Count; a++)
        {
            text[a].gameObject.SetActive(false);
        }
        if (content.Length == 1) text[0].text = "";
        contentScroll = content[0];
        characterTempo = 0;
        if (Button.mode == Button.Mode.Keyboard)
        {
            selectedIndex = 0;
            selectedText = content[0];
        }
        else
        {
            selectedIndex = -1;
            selectedText = "";
        }
        UpdateSelectedText();
    }

    public void ShowWolfInput()
    {
        content.SetActive(true);
        background.color = backgroundWolf;
        contentCount = 0;
        EnsureTextCount(1);
        for (int a = 0; a < text.Count; a++)
        {
            text[a].gameObject.SetActive(a == 0);
        }
        text[0].text = "";
        text[0].fontStyle = FontStyle.Normal;
        inputText = "";
        selectedIndex = 0;
        selectedText = "";
        UpdateSelectedText();
    }

    public void SetOverlay(params Sprite[] sprite)
    {
        if (sprite != null && sprite.Length > 0)
        {
            overlay.SetActive(true);
            overlayImage.sprite = sprite[0];
            overlaySprites = sprite;
            overlayTempo = 0;
            overlayIndex = 0;
        }
        else
        {
            overlay.SetActive(false);
            overlaySprites = null;
        }
    }

    public bool EndTextAnimation()
    {
        if (contentCount == 1 && characterTempo < contentScroll.Length)
        {
            characterTempo = contentScroll.Length;
            text[0].text = contentScroll;
            return true;
        }
        return false;
    }

    public void HideText()
    {
        content.SetActive(false);
        SetOverlay(null);
    }

    void UpdateSelectedText()
    {
        for (int a = 0; a < text.Count; a++)
        {
            if (contentCount == 1 || Button.mode == Button.Mode.Touch || a == selectedIndex)
            {
                text[a].color = Color.white;
            }
            else
            {
                text[a].color = new Color(1, 1, 1, unselectedTextAlpha);
            }
        }
    }
}
