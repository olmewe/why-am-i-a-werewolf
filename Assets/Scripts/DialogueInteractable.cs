﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DialogueInteractable : MonoBehaviour, IInteractable
{
    [Header("sprites")]
    public Sprite[] idle;
    public float idleFrameDuration;
    public enum TalkingOrientation { None, LookingLeft, LookingRight }
    public TalkingOrientation talkingOrientation;
    public Sprite[] sprites;
    public Sprite[] overlay;

    [Header("internal references")]
    public SpriteRenderer sprite;

    protected Dialogue dialogue;
    protected bool canInteract = true;

    bool idleAnimate = true;
    float idleTempo = 0;
    int idleIndex = 0;

    void Update()
    {
        if (idleAnimate && idle != null && idle.Length > 0)
        {
            idleTempo += Time.deltaTime;
            if (idleTempo >= idleFrameDuration)
            {
                idleTempo = 0;
                idleIndex = (idleIndex + 1) % idle.Length;
            }
            sprite.sprite = idle[idleIndex];
        }
        else
        {
            idleTempo = 0;
            idleIndex = 0;
        }
    }

    public void Interact()
    {
        if (canInteract) StartCoroutine(Coroutine());
    }

    IEnumerator Coroutine()
    {
        dialogue = Dialogue.instance;
        yield return StartDialogue();
        idleAnimate = true;
        dialogue.HideText();
    }

    const float actionTolerance = .1f;

    protected bool AdvanceInput()
    {
        if (Button.Press(Button.action)) return true;
        if (!Button.pointerNew) return false;
        return Dialogue.instance.EnsureCanAdvanceWithPointer();
    }

    protected IEnumerator NPCText(string content, bool italic = false)
    {
        yield return null;
        dialogue.ShowNPCText(content, italic);
        yield return new WaitForSeconds(actionTolerance);
        while (!AdvanceInput()) yield return null;
        if (dialogue.EndTextAnimation())
        {
            yield return new WaitForSeconds(actionTolerance);
            while (!AdvanceInput()) yield return null;
        }
    }

    protected IEnumerator WolfText(params string[] content)
    {
        yield return null;
        dialogue.ShowWolfText(content);
        yield return new WaitForSeconds(actionTolerance);
        while (!AdvanceInput()) yield return null;
        if (dialogue.EndTextAnimation())
        {
            yield return new WaitForSeconds(actionTolerance);
            while (!AdvanceInput()) yield return null;
        }
    }

    protected IEnumerator WolfInput()
    {
        yield return null;
        dialogue.ShowWolfInput();
        yield return new WaitForSeconds(actionTolerance);
        while (!AdvanceInput()) yield return null;
    }

    protected void SetSprite(Sprite sprite)
    {
        this.sprite.sprite = sprite;
        if (idleAnimate)
        {
            idleAnimate = false;
            if (talkingOrientation != TalkingOrientation.None)
            {
                float dist = Wolf.instance.transform.position.x - transform.position.x;
                if (talkingOrientation == TalkingOrientation.LookingRight) dist = -dist;
                this.sprite.flipX = dist > 0;
            }
        }
    }
    protected void SetSprite(int index) => SetSprite(sprites[index]);

    protected void SetOverlay() => dialogue.SetOverlay();
    protected void SetOverlay(params Sprite[] sprite) => dialogue.SetOverlay(sprite);
    protected void SetOverlay(params int[] index)
    {
        var sprite = new List<Sprite>();
        for (int a = 0; a < index.Length; a++)
        {
            if (index[a] >= 0) sprite.Add(overlay[index[a]]);
        }
        dialogue.SetOverlay(sprite.ToArray());
    }

    protected IEnumerator GetItem(string name, bool showOverlay = false)
    {
        if (showOverlay) SetOverlay(overlay);
        yield return WolfText("<< you got “" + name + "” >>");
        SetOverlay();
        if (showOverlay) Puzzle.instance?.UnlockPiece();
    }

    protected abstract IEnumerator StartDialogue();
}
