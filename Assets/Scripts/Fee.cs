﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fee : MonoBehaviour
{
    public static Fee instance;
    public Text text;

    int fee = 0;

    void Awake()
    {
        instance = this;
    }

    public void AddFee(int amount)
    {
        fee += amount;
        text.text = "-$" + fee;
    }
}
