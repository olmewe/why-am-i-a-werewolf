﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    public static Puzzle instance { get; private set; }

    [Header("background")]
    public SpriteRenderer background;
    public Sprite backgroundFull;

    [Header("puzzle piece")]
	public float pieceGrabTolerance;
	public float pieceSnapTolerance;
    //public Vector2 pieceSpawnOffset;
    //public Vector2 pieceSpawnArea;

    [Header("cursor")]
    public Transform cursor;
    public SpriteRenderer cursorSprite;
    public float cursorVelocity;
    public Vector2 cursorRange;
    public Vector2 cursorDefaultPosition;
    public Vector2 cursorHiddenPosition;
    public Sprite pawOpen;
    public Sprite pawClosed;
    public float grabAlpha;

    [Header("mask")]
    public Transform mask;
    public float maskMinHeight;
    public float maskMaxHeight;
    public float maskVelocity;
    public Vector2 pointerToggleCorner;

    [Header("floating piece")]
    public Transform floatingPiece;
    public Vector2 floatingPieceEndPosition;

    [Header("end cutscene")]
    public float endCutsceneStartDuration;
    public DialogueInteractable endCutsceneDialogue;

    [Header("general internal references")]
    public GameObject content;
    public PuzzlePiece[] pieces;

    //Vector2[] idealPosition;
    bool[] unlocked;
    bool[] completed;
    List<int> locked;

    public bool presenting { get; private set; }

    Vector2 cursorPosition;
    float cursorSmoothTempo = 0;
    int grabbing = -1;
    Vector2 grabOffset;
    float grabAlphaTempo;

    float maskTempo = 0;

    float floatingPieceTempo = 0;

    bool pawPointerAllowed = false;
    bool pawPointerEnabled = false;

    float endCutsceneStartTempo = 0;
    bool endCutscene;

    bool skipUpdate = false;

    void Awake()
    {
        instance = this;

        unlocked = new bool[pieces.Length];
        completed = new bool[pieces.Length];
        locked = new List<int>();

        //idealPosition = new Vector2[pieces.Length];
        for (int a = 0; a < pieces.Length; a++)
        {
            //idealPosition[a] = pieces[a].transform.position;
            pieces[a].gameObject.SetActive(false);
            locked.Add(a);
        }
        
        content.SetActive(true);
        UpdateMask();

        cursorPosition = cursorDefaultPosition;
        grabAlphaTempo = 1;
        UpdateCursor();
    }

    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.P)) UnlockPieces(pieces.Length);
#endif

        bool wasPresenting = presenting;

        if (endCutscene)
        {
            if (endCutsceneStartTempo > 0)
            {
                endCutsceneStartTempo -= Time.deltaTime;
                if (endCutsceneStartTempo <= 0)
                {
                    if (endCutsceneDialogue)
                    {
                        endCutsceneDialogue.Interact();
                    }
                    else
                    {
                        StopEndCutscene();
                    }
                }
            }
            presenting = endCutsceneStartTempo > 0;
        }
        
        if (!endCutscene && Dialogue.instance?.showingDialogue != true && ToggleScreenInput())
        {
            presenting = !presenting;
        }

        if (presenting && Dialogue.instance?.showingDialogue == true)
        {
            presenting = false;
        }

        if (presenting)
        {
            if (maskTempo < 1)
            {
                maskTempo += Time.deltaTime * maskVelocity;
                if (maskTempo > 1) maskTempo = 1;
                UpdateMask();
            }
        }
        else
        {
            if (maskTempo > 0)
            {
                maskTempo -= Time.deltaTime * maskVelocity;
                if (maskTempo < 0) maskTempo = 0;
                UpdateMask();
            }
        }

        if (floatingPieceTempo > 0)
        {
            floatingPieceTempo -= Time.deltaTime;
            if (floatingPieceTempo <= 0)
            {
                floatingPieceTempo = 0;
                floatingPiece.gameObject.SetActive(false);
            }
            else
            {
                floatingPiece.gameObject.SetActive(true);
                float tp = floatingPieceTempo;
                tp = (3 - 2 * tp) * tp * tp;
                floatingPiece.localPosition = Vector3.Lerp(floatingPieceEndPosition, Vector3.zero, tp);
                float ts = floatingPieceTempo;
                ts = (2 - ts) * ts;
                ts = (2 - ts) * ts;
                ts = (2 - ts) * ts;
                floatingPiece.localScale = Vector3.one * ts;
            }
        }

        if (presenting && !endCutscene)
        {
            if (skipUpdate)
            {
                skipUpdate = false;
            }
            else if (Button.pointerAvailable)
            {
                if (Button.pointerNew) pawPointerAllowed = true;
                if (pawPointerAllowed)
                {
                    cursorPosition = Camera.main.ScreenToWorldPoint(Button.pointerPosition) - transform.position;
                    ClampCursor();
                    if (!pawPointerEnabled)
                    {
                        ReleasePiece(false);
                        TryGrabPiece();
                        pawPointerEnabled = true;
                        cursorSmoothTempo = 1;
                    }
                }
            }
            else
            {
                if (pawPointerEnabled)
                {
                    ReleasePiece();
                    pawPointerEnabled = false;
                    cursorSmoothTempo = 1;
                    cursorPosition = cursorDefaultPosition;
                }
                var axis = Button.axis;
                if (axis != Vector2.zero)
                {
                    cursorPosition += axis * (cursorVelocity * Time.deltaTime);
                    ClampCursor();
                }
                if (Button.Press(Button.action))
                {
                    if (grabbing < 0)
                    {
                        TryGrabPiece();
                    }
                    else
                    {
                        ReleasePiece();
                    }
                }
            }
        }
        else
        {
            ReleasePiece(false);
            cursorPosition = endCutscene ? cursorHiddenPosition : cursorDefaultPosition;
            pawPointerAllowed = false;
            pawPointerEnabled = false;
            skipUpdate = true;
        }

        if (grabbing >= 0)
        {
            var grab = pieces[grabbing];
            var grabPos = grab.transform.localPosition;
            grabPos.x = cursorPosition.x - grabOffset.x;
            grabPos.y = cursorPosition.y - grabOffset.y;
            grab.transform.localPosition = grabPos;
        }

        if (wasPresenting && !presenting) cursorSmoothTempo = 1;

        UpdateCursor();
    }

    public bool UnlockPieces(int amount)
    {
        bool unlocked = false;
        for (int a = 0; a < amount; a++)
        {
            if (UnlockRandomPiece()) unlocked = true;
        }
        if (unlocked)
        {
            floatingPieceTempo = 1;
            Audio.instance?.PlayJingle();
            return true;
        }
        return false;
    }

    public bool UnlockPiece()
    {
        if (UnlockRandomPiece())
        {
            floatingPieceTempo = 1;
            Audio.instance?.PlayJingle();
            return true;
        }
        return false;
    }

    bool UnlockRandomPiece()
    {
        if (locked.Count == 0) return false;
        int random = Random.Range(0, locked.Count);
        int index = locked[random];
        locked.RemoveAt(random);

        unlocked[index] = true;
        pieces[index].gameObject.SetActive(true);
        
        //pieces[index].transform.localPosition = new Vector2(
        //    (pieceSpawnOffset.x + pieceSpawnArea.x * Random.Range(-.5f, .5f)) * (Random.value > .5f ? 1 : -1),
        //    pieceSpawnOffset.y + pieceSpawnArea.y * Random.Range(-.5f, .5f)
        //);
        
        return true;
    }

    void ClampCursor()
    {
        var halfRange = cursorRange * .5f;
        cursorPosition.x = Mathf.Clamp(cursorPosition.x, -halfRange.x, halfRange.x);
        cursorPosition.y = Mathf.Clamp(cursorPosition.y, -halfRange.y, halfRange.y);
    }

    void UpdateCursor()
    {
        if (cursorSmoothTempo > 0)
        {
            float duration = endCutscene ? 7 : 2;
            cursorSmoothTempo -= Time.deltaTime / duration;
            if (cursorSmoothTempo < 0) cursorSmoothTempo = 0;
        }
        var pos = cursor.localPosition;
        pos.x = Mathf.Lerp(cursorPosition.x, pos.x, cursorSmoothTempo);
        pos.y = Mathf.Lerp(cursorPosition.y, pos.y, cursorSmoothTempo);
        cursor.localPosition = pos;
        cursor.localEulerAngles = new Vector3(0, 0, Mathf.Atan2(pos.y + 40, pos.x) * Mathf.Rad2Deg - 90);
        if (grabbing < 0)
        {
            if (grabAlphaTempo > 0)
            {
                grabAlphaTempo -= Time.deltaTime * 2;
                if (grabAlphaTempo < 0) grabAlphaTempo = 0;
            }
        }
        else
        {
            if (grabAlphaTempo < 1)
            {
                grabAlphaTempo += Time.deltaTime * 2;
                if (grabAlphaTempo > 1) grabAlphaTempo = 1;
            }
        }
        cursorSprite.sprite = grabbing < 0 ? pawOpen : pawClosed;
        float t = grabAlphaTempo;
        t = (3 - 2 * t) * t * t;
        cursorSprite.color = new Color(1, 1, 1, Mathf.Lerp(1, grabAlpha, t));
    }

    void TryGrabPiece()
    {
        if (grabbing >= 0) return;
        float minDistance = 0;
        for (int a = 0; a < pieces.Length; a++)
        {
            if (completed[a] || !unlocked[a]) continue;
            Vector2 cursorPositionGlobal = transform.TransformPoint(cursorPosition);
            var point = pieces[a].GetComponent<Collider2D>().ClosestPoint(cursorPositionGlobal);
            var pointDistance = Vector2.SqrMagnitude(point - cursorPositionGlobal);
            if (pointDistance > pieceGrabTolerance * pieceGrabTolerance) continue;
            var offset = cursorPosition - (Vector2)pieces[a].transform.localPosition;
            float distance = Vector2.SqrMagnitude(offset);
            if (grabbing < 0 || minDistance > distance)
            {
                minDistance = distance;
                grabbing = a;
                grabOffset = offset;
            }
        }
        if (grabbing >= 0)
        {
            pieces[grabbing].MoveFront(true);
            Audio.instance?.PlaySelect();
        }
    }

    void ReleasePiece(bool apply = true)
    {
        if (grabbing < 0) return;
        pieces[grabbing].MoveFront(false);
        if (apply)
        {
            var offset = (Vector2)pieces[grabbing].transform.localPosition;// - idealPosition[grabbing];
            if (Vector2.SqrMagnitude(offset) <= pieceSnapTolerance * pieceSnapTolerance)
            {
                //pieces[grabbing].transform.localPosition = idealPosition[grabbing];
                pieces[grabbing].transform.localPosition = Vector3.zero;
                pieces[grabbing].Animate();
                completed[grabbing] = true;
            }
            Audio.instance?.PlaySelect();
            CheckEnd();
        }
        grabbing = -1;
    }

    void UpdateMask()
    {
        var scale = mask.localScale;
        float t = maskTempo;
        t = (3 - 2 * t) * t * t;
        scale.y = Mathf.Lerp(maskMinHeight, maskMaxHeight, t);
        mask.localScale = scale;
    }

    bool ToggleScreenInput()
    {
        if (Button.Press(Button.menu)) return true;
        if (!Button.pointerNew) return false;
        var cam = Camera.main;
        Vector2 point = cam.ScreenToWorldPoint(Button.pointerPosition) - cam.transform.position;
        float x = Mathf.Abs(pointerToggleCorner.x);
        float y = Mathf.Abs(pointerToggleCorner.y);
        return presenting ? (point.x > x && point.y < -y) : (point.x < -x && point.y > y);
    }

    void CheckEnd()
    {
        for (int a = 0; a < completed.Length; a++)
        {
            if (!completed[a]) return;
        }

        endCutscene = true;
        endCutsceneStartTempo = endCutsceneStartDuration;
        cursorSmoothTempo = 1;

        background.sprite = backgroundFull;

        for (int a = 0; a < pieces.Length; a++)
        {
            pieces[a].Hide();
        }
    }

    public void StopEndCutscene() => endCutscene = false;
}
