﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePiece : MonoBehaviour
{
    float tempo;
    bool hide;

    SpriteRenderer sprite;

    int sort;

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
        sort = sprite.sortingOrder;
    }

    void Update()
    {
        if (tempo > 0)
        {
            tempo -= Time.deltaTime * 4;
            if (tempo <= 0)
            {
                tempo = 0;
                if (hide) sprite.enabled = false;
            }
            float t = tempo > .5f ? (1 - tempo) * 2 : tempo * 2;
            transform.localScale = Vector3.one * (1 + t * .07f);
            sprite.color = Color.Lerp(Color.white, Color.yellow, t * .75f);
        }
    }

    public void Animate()
    {
        tempo = 1;
    }

    public void MoveFront(bool pls)
    {
        sprite.sortingOrder = sort + (pls ? 1 : 0);
    }

    public void Hide()
    {
        hide = true;
        if (tempo <= 0) sprite.enabled = false;
    }
}
