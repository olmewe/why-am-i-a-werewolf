﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowResizer : MonoBehaviour
{
    int width;
    int height;
    public const float ratio = 16f / 9f;
    
    void Awake()
    {
        width = Screen.width;
        height = Screen.height;
        var currentRatio = (float)width / height;
        Rect rect;
        if (Mathf.Approximately(currentRatio, ratio))
        {
            rect = new Rect(0, 0, 1, 1);
        }
        else if (currentRatio > ratio)
        {
            var propSize = ratio / currentRatio;
            var propMin = (1 - propSize) / 2f;
            rect = new Rect(propMin, 0, propSize, 1);
        }
        else
        {
            var propSize = currentRatio / ratio;
            var propMin = (1 - propSize) / 2f;
            rect = new Rect(0, propMin, 1, propSize);
        }
        GetComponent<Camera>().rect = rect;
    }

    void Update()
    {
        if (width != Screen.width || height != Screen.height) Awake();
    }
}