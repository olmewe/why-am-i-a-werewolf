﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolf : MonoBehaviour
{
    public static Wolf instance;

    [Header("properties")]
    public float velocity;
    public float interactableDistance;
    public float interactableMaxDistance;
    public Vector2 interactableArea;

    [Header("sprites")]
    public SpriteRenderer sprite;
    public Sprite[] sprites;
    public float animVelocity;

    [Header("general internal references")]
    public BoxCollider2D hitbox;
    public Transform touchShadow;

    bool walking;
    Vector2 lookDirection = Vector2.down;
    float walkTempo;
    int animDirection;
    int animWalk;

    bool canSetGoal;
    bool hasGoal;
    float goalTempo;
    Vector2 goalPosition;

    readonly static Collider2D[] colliderResults = new Collider2D[64];

    public static bool cameToTerms = false;

    bool skipUpdate = false;

    void Awake()
    {
        instance = this;

        canSetGoal = false;
        hasGoal = false;
        goalTempo = 0;
        SetTouchShadow();
    }

    void Update()
    {
        if (Dialogue.instance?.showingDialogue == true || Puzzle.instance?.presenting == true)
        {
            skipUpdate = true;
            LogicReset();
        }
        else if (skipUpdate)
        {
            skipUpdate = false;
            LogicReset();
        }
        else
        {
            LogicUpdate();
        }

        if (walking)
        {
            walkTempo = (walkTempo + Time.deltaTime * animVelocity) % 4;
            switch (Mathf.FloorToInt(walkTempo))
            {
                case 0: animWalk = 1; break;
                case 2: animWalk = 2; break;
                default: animWalk = 0; break;
            }
        }
        else
        {
            walkTempo = 0;
            animWalk = 0;
        }

        float angle = Mathf.Atan2(lookDirection.y, lookDirection.x) * 8 / (2f * Mathf.PI);
        animDirection = (Mathf.RoundToInt(angle) + 2) % 8;
        if (animDirection < 0) animDirection += 8;

        sprite.sprite = sprites[animDirection * 3 + animWalk];

        if (hasGoal)
        {
            if (goalTempo < 1)
            {
                goalTempo += Time.deltaTime * 10;
                if (goalTempo > 1) goalTempo = 1;
            }
            SetTouchShadow();
        }
        else
        {
            if (goalTempo > 0)
            {
                goalTempo -= Time.deltaTime * 3;
                if (goalTempo < 0) goalTempo = 0;
                SetTouchShadow();
            }
        }
    }

    void LogicReset()
    {
        walking = false;
        canSetGoal = false;
        hasGoal = false;
    }

    void LogicUpdate()
    {
        walking = false;
        
        bool interacted = false;

        var axis = Button.axis;
        if (axis != Vector2.zero)
        {
            walking = true;
            lookDirection = axis;

            float distance = velocity * Time.deltaTime;
            if (!TryWalk(axis, distance))
            {
                var horizontal = new Vector2(axis.x, 0);
                var vertical = new Vector2(0, axis.y);
                TryWalk(horizontal, distance);
                TryWalk(vertical, distance);
            }

            canSetGoal = false;
            hasGoal = false;
        }
        else
        {
            if (Button.pointerNew)
            {
                if (interacted)
                {
                    canSetGoal = true;
                }
                else
                {
                    var lookPoint = Vector2.zero;

                    Vector2 point = Camera.main.ScreenToWorldPoint(Button.pointerPosition);
                    var delta = point - (Vector2)transform.position;
                    if (delta.sqrMagnitude <= interactableMaxDistance * interactableMaxDistance)
                    {
                        var interactable = TryInteract(point);
                        if (interactable != null)
                        {
                            interacted = true;
                            if (interactable is MonoBehaviour obj)
                            {
                                lookPoint = obj.transform.position;
                            }
                            else
                            {
                                lookPoint = point;
                            }
                        }
                    }
                    if (interacted)
                    {
                        var lookDelta = lookPoint - (Vector2)transform.position;
                        float lookSqrDistance = lookDelta.sqrMagnitude;
                        if (!Mathf.Approximately(lookSqrDistance, 0))
                        {
                            lookDirection = lookDelta / Mathf.Sqrt(lookSqrDistance);
                        }
                        canSetGoal = false;
                        hasGoal = false;
                    }
                    else
                    {
                        canSetGoal = true;
                    }
                }
            }
            if (Button.pointerAvailable && canSetGoal)
            {
                hasGoal = true;
                goalPosition = Camera.main.ScreenToWorldPoint(Button.pointerPosition);
                if (Button.pointerNew) goalTempo = 0;
            }
            if (hasGoal)
            {
                var delta = goalPosition - (Vector2)transform.position;
                float sqrDistanceTotal = delta.sqrMagnitude;
                if (Mathf.Approximately(sqrDistanceTotal, 0))
                {
                    hasGoal = false;
                }
                else
                {
                    float distanceTotal = Mathf.Sqrt(sqrDistanceTotal);
                    var direction = delta / distanceTotal;
                    float maxDistance = velocity * Time.deltaTime;
                    float distance = Mathf.Min(distanceTotal, maxDistance);

                    walking = true;
                    lookDirection = direction;

                    if (!TryWalk(direction, distance))
                    {
                        if (!Mathf.Approximately(direction.x, 0) && !Mathf.Approximately(direction.y, 0))
                        {
                            var directionX = new Vector2(Mathf.Sign(direction.x), 0);
                            distance = Mathf.Min(Mathf.Abs(delta.x), maxDistance);
                            if (!TryWalk(directionX, distance))
                            {
                                var directionY = new Vector2(0, Mathf.Sign(direction.y));
                                distance = Mathf.Min(Mathf.Abs(delta.y), maxDistance);
                                if (!TryWalk(directionY, distance))
                                {
                                    hasGoal = false;
                                }
                            }
                        }
                        else
                        {
                            hasGoal = false;
                        }
                    }
                }
            }
        }

        if (!interacted && Button.Press(Button.action))
        {
            var point = (Vector2)transform.position + lookDirection * interactableDistance;
            interacted = TryInteract(point) != null;
        }
    }

    bool TryWalk(Vector2 direction, float distance)
    {
        bool hitboxEnabled = hitbox.enabled;
        hitbox.enabled = false;
        var collider = Physics2D.BoxCast(
            (Vector2)hitbox.transform.position + hitbox.offset,
            hitbox.size,
            0,
            direction,
            distance,
            1
        ).collider;
        bool collide = collider && collider.isActiveAndEnabled;
        hitbox.enabled = hitboxEnabled;
        if (!collide)
        {
            transform.position += (Vector3)(direction * distance);
            return true;
        }
        return false;
    }

    IInteractable TryInteract(Vector2 point)
    {
        bool hitboxEnabled = hitbox.enabled;
        hitbox.enabled = false;
        var count = Physics2D.OverlapBox(point, interactableArea, 0, new ContactFilter2D().NoFilter(), colliderResults);
        hitbox.enabled = hitboxEnabled;
        for (int a = 0; a < count; a++)
        {
            var interactable = colliderResults[a].GetComponentInParent<IInteractable>();
            if (interactable != null && (interactable as MonoBehaviour).isActiveAndEnabled)
            {
                interactable.Interact();
                return interactable;
            }
        }
        return null;
    }

    void SetTouchShadow()
    {
        if (goalTempo > 0)
        {
            touchShadow.gameObject.SetActive(true);
            touchShadow.position = goalPosition;
            float t = 1 - goalTempo;
            touchShadow.localScale = Vector3.one * (1 - t * t);
        }
        else
        {
            touchShadow.gameObject.SetActive(false);
        }
    }
}
